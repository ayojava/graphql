package org.javasoft.graphql.mutation

import graphql.kickstart.tools.GraphQLMutationResolver
import org.apache.commons.lang3.RandomStringUtils
import org.javasoft.graphql.*
import org.springframework.stereotype.Component

@Component
class MemberEntityMutation(val memberRepository: MemberRepository) : GraphQLMutationResolver {

    fun createMember(createMemberRequest: CreateMemberRequest) : Member {
        val memberEntity = MemberEntity(firstName = createMemberRequest.firstName, lastName = createMemberRequest.lastName,
            registrationNo = RandomStringUtils.randomNumeric(6), emailAddress = createMemberRequest.emailAddress
        )
        return memberRepository.save(memberEntity).toMember()
    }
}