package org.javasoft.graphql.mutation

import graphql.kickstart.tools.GraphQLMutationResolver
import org.apache.commons.lang3.RandomStringUtils
import org.javasoft.graphql.*
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.util.*

@Component
class BorrowBookMutation(val borrowedBookRepository: BorrowedBookRepository,
                         val memberRepository: MemberRepository,
                         val bookRepository: BookRepository
): GraphQLMutationResolver {

    fun createBorrowBook(borrowBookRequest: BorrowBookRequest) : BorrowBook {
       val bookEntity =  bookRepository.findByBookCode(borrowBookRequest.bookCode) ?: throw BookException("Book with code = ${borrowBookRequest.bookCode}  not found" ,
            mapOf("bookCode" to borrowBookRequest.bookCode))
        if(bookEntity.status != "AV"){
            throw BookException("Book with code = ${borrowBookRequest.bookCode}  not available" ,
                mapOf("bookCode" to borrowBookRequest.bookCode))
        }
        val memberEntity = memberRepository.findByMemberId(borrowBookRequest.memberId)
            ?: throw MemberException("Member = ${borrowBookRequest.memberId}  not found" ,
                mapOf("memberId" to borrowBookRequest.memberId))

        val borrowedBookEntity = BorrowedBookEntity(
            returnCode = RandomStringUtils.randomAlphabetic(4).uppercase(Locale.getDefault()),
            borrowedBook = bookEntity,
            borrowedBy = memberEntity,
            borrowedDate = LocalDate.now(),
            dueDate = LocalDate.now().plusDays(10)
        )

        val aBorrowedBookEntity = borrowedBookRepository.save(borrowedBookEntity)
        bookRepository.save(bookEntity.copy(status = "BW"))

       return  mapBorrowedBookEntity(aBorrowedBookEntity)
    }

    fun returnBorrowBook(returnBook: ReturnBook) : ReturnBookResponse{
        val borrowedBookEntity = borrowedBookRepository.findByReturnCodeAndBorrowedBy_MemberId(returnBook.returnCode,returnBook.memberId) ?:
        throw BorrowedBookException("No matching records found " , mapOf("returnCode" to returnBook.returnCode,"memberId" to returnBook.memberId))

        bookRepository.save(borrowedBookEntity.borrowedBook.copy(status = "AV"))

        borrowedBookRepository.delete(borrowedBookEntity)
        return ReturnBookResponse(returnBook.returnCode,returnBook.memberId,"Book Returned successfully")
    }
}