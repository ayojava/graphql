package org.javasoft.graphql.mutation

import graphql.kickstart.tools.GraphQLMutationResolver
import mu.KotlinLogging
import org.javasoft.graphql.*
import org.springframework.stereotype.Component

private val log = KotlinLogging.logger {}

@Component
class BookShelveEntityMutation(val bookShelveRepository: BookShelveRepository): GraphQLMutationResolver {

    fun createBookShelve(shelveCode : String,shelveName: String ) : BookShelveData {
        if(bookShelveRepository.existsBookShelveEntityByShelveCode(shelveCode)){
            throw BookShelveException("BookShelve with code = ${shelveCode}  already exists in the database" ,
                mapOf("shelveCode" to shelveCode))
        }
        val aBookShelve = BookShelveEntity(shelveCode = shelveCode , shelveName = shelveName)
        bookShelveRepository.save(aBookShelve).let {
           return  BookShelveData(shelveCode =it.shelveCode , shelveName = it.shelveName , bookInfoList = null, bookCount = 0)
        }
    }
}