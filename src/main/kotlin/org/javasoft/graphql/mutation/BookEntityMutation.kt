package org.javasoft.graphql.mutation

import graphql.kickstart.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import mu.KotlinLogging
import net.bytebuddy.utility.RandomString
import org.apache.commons.lang3.RandomStringUtils
import org.javasoft.graphql.*
import java.text.SimpleDateFormat
import java.time.ZoneId
import java.util.*

private val log = KotlinLogging.logger {}

@Component
class BookEntityMutation(val bookRepository: BookRepository, val bookShelveRepository: BookShelveRepository) : GraphQLMutationResolver{

    fun createBook(createBookRequest : CreateBookRequest) : List<Book> {
        val bookShelveEntityOpt: BookShelveEntity? = bookShelveRepository.findByShelveCode(createBookRequest.shelveCode)
        val bookShelveEntity = bookShelveEntityOpt ?: throw BookShelveException("BookShelve with code = ${createBookRequest.shelveCode}  not found" ,
        mapOf("shelveCode" to createBookRequest.shelveCode))

        val bookList = mutableListOf<Book>()
        val bookISBN = UUID.randomUUID().toString()

        for (i in 1..createBookRequest.noOfCopies){
            val bookEntity = BookEntity(
                bookName = createBookRequest.bookName,
                bookCode = createBookRequest.shelveCode.uppercase() + RandomStringUtils.randomNumeric(3) ,
                bookISBN = bookISBN,
                bookShelveEntity = bookShelveEntity,
                publisherDate = createBookRequest.publisherDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                publisherName = createBookRequest.publisherName,
                bookAuthor = createBookRequest.bookAuthor,
                status = "AV"
            )

            val (bookId, bookName, bookCode, bookAuthor, publisherName, isbn, publisherDate, status, shelveEntity, createdAt) = bookRepository.save(
                bookEntity
            )
            val (_, shelveCode, shelveName) = shelveEntity
            val aBook = Book(bookId!!,bookName,bookCode,bookAuthor,publisherName,isbn, publisherDate.toString(),status,
                SimpleDateFormat("dd-MMMM-yyyy").format(createdAt), BookShelve(shelveCode, shelveName))
            bookList.add(aBook)
        }
        return bookList
    }
}