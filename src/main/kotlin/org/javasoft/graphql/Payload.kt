package org.javasoft.graphql

import java.time.LocalDate
import java.util.*

data class BookShelve( val shelveCode: String, val shelveName : String )

data class Book(val bookId : Long ,val bookName : String,val bookCode : String ,val bookAuthor : String , val publisherName : String,
val bookISBN : String,val publisherDate : String,val status : String ,val createdAt : String , val bookShelve: BookShelve)

data class CreateBookRequest(val bookName : String , val bookAuthor : String, val publisherName : String,
val publisherDate : Date ,val noOfCopies : Int , val shelveCode : String )

data class BookInfo(val bookName : String,val bookCode : String ,val bookAuthor : String , val publisherName : String,
                val bookISBN : String,val publisherDate : String,val noOfCopies : Int)

data class BookShelveData(val shelveCode: String, val shelveName: String, val bookInfoList: List<BookInfo>?, var bookCount: Int? =0)

data class CreateMemberRequest(val firstName : String,val lastName : String,val emailAddress : String)

data class Member(val memberId : Long , val firstName : String,val lastName : String,val registrationNo : String ,val emailAddress : String)

data class BorrowBookRequest(val bookCode: String , val memberId: Long)

data class BookDetail(val bookName : String,val bookCode : String,val bookAuthor : String , val publisherName : String,
                      val bookISBN : String,val publisherDate : String)

data class MemberDetail(val firstName : String,val lastName : String, val registrationNo : String ,val emailAddress : String)

data class BookReceipt(val returnCode : String ,val borrowDate : LocalDate , val returnDate: LocalDate)

data class BorrowBook(  val bookDetail: BookDetail , val memberDetail: MemberDetail , val bookReceipt: BookReceipt)

data class ReturnBook(val returnCode : String , val memberId: Long)

data class ReturnBookResponse(val returnCode : String , val memberId: Long , val returnStatus: String)