package org.javasoft.graphql

import graphql.ErrorClassification
import graphql.ErrorType

open class BookShelveException(errorMessage: String? = "",
                               private val parameters: Map<String, Any>? = mutableMapOf()): GraphQLException(errorMessage) {
    override val message: String?
        get() = super.message

    override fun getExtensions(): MutableMap<String, Any> {
        return mutableMapOf("parameters" to (parameters ?: mutableMapOf()))
    }

    override fun getErrorType(): ErrorClassification {
        return ErrorType.DataFetchingException
    }
}

open class BorrowedBookException(errorMessage: String? = "",
                         private val parameters: Map<String, Any>? = mutableMapOf()): GraphQLException(errorMessage) {
    override val message: String?
        get() = super.message

    override fun getExtensions(): MutableMap<String, Any> {
        return mutableMapOf("parameters" to (parameters ?: mutableMapOf()))
    }

    override fun getErrorType(): ErrorClassification {
        return ErrorType.DataFetchingException
    }
}

open class BookException(errorMessage: String? = "",
                               private val parameters: Map<String, Any>? = mutableMapOf()): GraphQLException(errorMessage) {
    override val message: String?
        get() = super.message

    override fun getExtensions(): MutableMap<String, Any> {
        return mutableMapOf("parameters" to (parameters ?: mutableMapOf()))
    }

    override fun getErrorType(): ErrorClassification {
        return ErrorType.DataFetchingException
    }
}

open class MemberException(errorMessage: String? = "",
                         private val parameters: Map<String, Any>? = mutableMapOf()): GraphQLException(errorMessage) {
    override val message: String?
        get() = super.message

    override fun getExtensions(): MutableMap<String, Any> {
        return mutableMapOf("parameters" to (parameters ?: mutableMapOf()))
    }

    override fun getErrorType(): ErrorClassification {
        return ErrorType.DataFetchingException
    }
}