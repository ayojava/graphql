package org.javasoft.graphql

import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface BookRepository : CrudRepository<BookEntity, Long>, JpaSpecificationExecutor<BookEntity>{

    fun findByBookCode(bookCode : String) : BookEntity?
}

@Repository
interface BookShelveRepository : CrudRepository<BookShelveEntity, Long>, JpaSpecificationExecutor<BookShelveEntity>{

    fun findByShelveCode(shelveCode : String) : BookShelveEntity?

    fun existsBookShelveEntityByShelveCode(shelveCode : String) : Boolean
}

@Repository
interface BorrowedBookRepository : CrudRepository<BorrowedBookEntity, Long>, JpaSpecificationExecutor<BorrowedBookEntity>{

    fun findByReturnCodeAndBorrowedBy_MemberId(returnCode : String, memberId : Long) : BorrowedBookEntity?
}

@Repository
interface MemberRepository : CrudRepository<MemberEntity,Long>, JpaSpecificationExecutor<MemberEntity>{

    fun findAllByOrderByFirstNameAsc(): List<MemberEntity>

    fun findByMemberId(memberId : Long) : MemberEntity?
}