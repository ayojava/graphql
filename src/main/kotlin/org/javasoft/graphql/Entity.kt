package org.javasoft.graphql

import org.hibernate.annotations.CreationTimestamp
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Table(name = "book")
data class BookEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val bookId: Long ? = null,

    @NotBlank
    @Column(nullable = false,unique = false)
    val bookName : String,

    @NotBlank
    @Column(nullable = false,unique = true)
    val bookCode : String,

    val bookAuthor : String,

    val publisherName : String,

    val bookISBN : String ,

    val publisherDate : LocalDate,

    @NotBlank
    val status: String,


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bookShelveEntityId")
    val bookShelveEntity: BookShelveEntity,

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @CreationTimestamp
    val createdAt : Date?= null
)

@Entity
@Table(name = "bookShelve")
data class BookShelveEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val bookShelveId: Long ? = null,

    @NotBlank
    @Column(nullable = false,unique = true)
    val shelveCode : String,

    @NotBlank
    @Column(nullable = false,unique = true)
    val shelveName : String,

    @Column(nullable = true)
    @OneToMany(cascade = [CascadeType.ALL] , mappedBy = "bookShelveEntity" , fetch = FetchType.EAGER)
    val bookEntityList: List<BookEntity>? = null,

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @CreationTimestamp
    val createdAt : Date?= null
)

@Entity
@Table(name = "borrowedBook")
data class BorrowedBookEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val borrowedBookId: Long ? = null,

    @NotBlank
    @Column(nullable = false,unique = true)
    val returnCode : String,

    @OneToOne
    val borrowedBook : BookEntity,

    @ManyToOne
    val borrowedBy: MemberEntity,

    val borrowedDate : LocalDate,

    val dueDate : LocalDate,

)

@Entity
@Table(name = "member")
data class MemberEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val memberId: Long ? = null,

    @NotBlank
    @Column(nullable = false,unique = false)
    val firstName : String,

    @NotBlank
    @Column(nullable = false,unique = false)
    val lastName : String,

    val registrationNo : String ,

    val emailAddress : String ,

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @CreationTimestamp
    val createdAt : Date?= null
)