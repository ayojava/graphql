package org.javasoft.graphql.resolver

import graphql.kickstart.tools.GraphQLQueryResolver
import org.javasoft.graphql.Member
import org.javasoft.graphql.MemberRepository
import org.javasoft.graphql.toMember
import org.springframework.stereotype.Component

@Component
class MemberEntityResolver(val memberRepository: MemberRepository) : GraphQLQueryResolver {

    fun getMembers() : List<Member> {
       return  memberRepository.findAllByOrderByFirstNameAsc().map { it.toMember() }
    }
}