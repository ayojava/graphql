package org.javasoft.graphql.resolver

import graphql.kickstart.tools.GraphQLQueryResolver
import mu.KotlinLogging
import org.javasoft.graphql.BookEntity
import org.javasoft.graphql.BookInfo
import org.javasoft.graphql.BookShelveData
import org.javasoft.graphql.BookShelveRepository
import org.springframework.stereotype.Component

private val log = KotlinLogging.logger {}

@Component
class BookShelveEntityResolver(val bookShelveRepository: BookShelveRepository) : GraphQLQueryResolver {

    fun getBookShelve() : List<BookShelveData>{
        val bookShelveDataList = mutableListOf<BookShelveData>()
        return bookShelveRepository.findAll().mapTo(bookShelveDataList){
            BookShelveData(it.shelveCode,it.shelveName, it.bookEntityList?.let { aBookEntity -> groupBookEntityList(aBookEntity) }
                , it.bookEntityList?.size)
        }
    }

    private fun groupBookEntityList(bookEntityList : List<BookEntity>): List<BookInfo> {
        val bookInfoList = mutableListOf<BookInfo>()
        var groupByBookCode: Map<String, List<BookEntity>> = bookEntityList.groupBy { it.bookCode }
        groupByBookCode.forEach { key, value ->
            run {
                val bookEntity = value[0]
                bookInfoList.add(
                    BookInfo(
                        bookEntity.bookName,
                        bookEntity.bookCode,
                        bookEntity.bookAuthor,
                        bookEntity.publisherName,
                        bookEntity.bookISBN,
                        bookEntity.publisherDate.toString(),
                        value.size
                    )
                )
            }
        }
        return bookInfoList
    }
}




