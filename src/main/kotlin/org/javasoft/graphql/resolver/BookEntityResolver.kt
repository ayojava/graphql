package org.javasoft.graphql.resolver


import graphql.kickstart.tools.GraphQLQueryResolver
import org.javasoft.graphql.Book
import org.javasoft.graphql.BookException
import org.javasoft.graphql.BookRepository
import org.javasoft.graphql.toBook
import org.springframework.stereotype.Component

@Component
class BookEntityResolver(val bookRepository: BookRepository) : GraphQLQueryResolver {

    fun getBooks() : List<Book> {
        var bookList = mutableListOf<Book>()
        return bookRepository.findAll().mapTo (bookList
        ) { it.toBook() }
    }

    fun findBook(id: Long): Book {
        return bookRepository.findById(id).map { it.toBook()}.orElseThrow { BookException("Book with id = ${id}  not found" ,
            mapOf("id" to id)) }
    }


}