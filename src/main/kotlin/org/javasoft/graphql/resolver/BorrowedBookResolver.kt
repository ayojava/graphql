package org.javasoft.graphql.resolver

import graphql.kickstart.tools.GraphQLQueryResolver
import org.javasoft.graphql.BorrowBook
import org.javasoft.graphql.BorrowedBookRepository
import org.javasoft.graphql.mapBorrowedBookEntity
import org.springframework.stereotype.Component

@Component
class BorrowedBookResolver (val borrowedBookRepository: BorrowedBookRepository) : GraphQLQueryResolver {

    fun getBorrowedBooks(): List<BorrowBook> {
        return borrowedBookRepository.findAll().map { mapBorrowedBookEntity(it) }
    }
}