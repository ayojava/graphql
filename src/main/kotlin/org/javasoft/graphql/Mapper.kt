package org.javasoft.graphql

fun BookEntity.toBook()=Book(
    bookId = bookId!!, bookAuthor = bookAuthor, bookCode = bookCode, bookISBN = bookISBN,
    bookName = bookName, publisherName = publisherName, publisherDate = publisherDate.toString(),
    createdAt = createdAt.toString(), status = status,bookShelve = BookShelve(bookShelveEntity.shelveCode,bookShelveEntity.shelveName)
)

fun MemberEntity.toMember() = Member(memberId = memberId!! , firstName = firstName , lastName = lastName , registrationNo = registrationNo ,
emailAddress = emailAddress)


fun mapBorrowedBookEntity(borrowedBookEntity: BorrowedBookEntity ) : BorrowBook{
    val (_, returnCode, borrowedBook, borrowedBy, borrowedDate, dueDate) = borrowedBookEntity
    val bookDetail = BookDetail(
        bookName = borrowedBook.bookName,
        bookCode = borrowedBookEntity.borrowedBook.bookCode,
        bookAuthor = borrowedBook.bookAuthor,
        publisherName = borrowedBook.publisherName,
        bookISBN = borrowedBook.bookISBN,
        publisherDate = borrowedBook.publisherDate.toString()
    )

    val memberDetail = MemberDetail(
        firstName = borrowedBy.firstName,
        lastName = borrowedBy.lastName,
        registrationNo = borrowedBy.registrationNo,
        emailAddress = borrowedBy.emailAddress
    )

    val bookReceipt = BookReceipt(returnCode, borrowedDate, dueDate)

    return BorrowBook(bookDetail,memberDetail,bookReceipt)
}