# Kotlin , Springboot  & GraphQL for a Sample Library Application

* Clone the repo and edit the application.yaml file for the database connection

* Run the application and open GraphiQL to run the below queries : http://localhost:8080/graphiql 
* Create a BookShelve
```graphql
mutation {
createBookShelve(shelveCode: "XXX", shelveName: "Science Research") {
    shelveCode
    shelveName
    bookInfoList 
        {
            bookId
            bookName
        }
    bookCount
    }
}
```

* Create Book(s) using the shelveCode returned earlier
```graphql
    mutation{
    createBook(createBookRequest : {
        bookName:"Phantom of The Phinx",
        bookCode:"DEF001",
        bookAuthor:"Java Deve",
        publisherName:"Dominion Publishing",
        publisherDate:"2013-01-01",
        noOfCopies:7,
        shelveCode : "DEF"
    }) {
        bookId
        bookName
        bookCode
        bookAuthor
        publisherName
        bookISBN
        publisherDate
        status
        createdAt
        bookShelve{
            shelveCode
            shelveName
        }
    }
}

```

* Get BookShelves

```graphql
query {
getBookShelve {
shelveCode
shelveName
bookInfoList {
bookCode
bookName
bookAuthor
noOfCopies
}
bookCount
}
}

```

* Create a member 
```graphql
    mutation{
    createMember(createMemberRequest : {
        firstName:"Ayodeji",
        lastName:"Ilori",
        emailAddress:"ayojava@gmail.com"
    }) {
        memberId
        firstName
        lastName
        registrationNo
        emailAddress

    }
}
```

*Borrow a book using the "bookCode" and "memberId"

```graphql
mutation{
createBorrowBook(borrowBookRequest : {
bookCode:"ABC004",
memberId:1
}) {
bookDetail {
bookName
bookCode
bookAuthor
publisherName
}
memberDetail{
firstName
lastName
registrationNo
}
bookReceipt{
returnCode
borrowDate
returnDate
}
}
}
```

* return a book borrowed
```graphql
mutation{
returnBorrowBook(returnBook : {
returnCode:"NPKZ",
memberId:1
}) {
returnCode
memberId
returnStatus
}
}

```
